import {TOGGLE_LOADING} from '../actions/constants';

export default (state = true, action) => {
    if (action.type === TOGGLE_LOADING) {
        return !state;
    }

    return state;
};
