import {combineReducers} from 'redux';
import loadingInProcess from './loadingInProcess';
import registrationInProcess from './registrationInProcess';

export default combineReducers({
    loadingInProcess,
    registrationInProcess,
});
