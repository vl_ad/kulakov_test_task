import {TOGGLE_REGISTRATION} from '../actions/constants';

export default (state = false, action) => {
    if (action.type === TOGGLE_REGISTRATION) {
        return !state;
    }

    return state;
};
