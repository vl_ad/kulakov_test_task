import React from 'react';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';

import '../../styles/textInput.scss';

function TextInput(props) {
    const {value, ...rest} = props;

    return (
        <TextField
            defaultValue={value}
            {...rest}
        />
    );
}

TextInput.propTypes = {
    value: PropTypes.string,
};

export default TextInput;
