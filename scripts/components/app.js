import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {hot} from 'react-hot-loader';

import {toggleLoading, toggleRegistration} from '../actions';
import Loader from './loader';
import Button from './button';
import TextInput from './textInput';
import Select from './select';
import Checkbox from './checkbox';
import SnackBar from './snackbar';
import ErrorComponent from './errorComponent';

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            countries: [],
            isSnackBarOpen: false,
            formError: {
                name: '',
                message: '',
            },
            name: '',
            dialCode: '',
            phone: '',
            email: '',
            password: '',
            country: '',
            passwordConfirmation: '',
            checkbox: false,
        };
    }

    toggleLoading = () => {
        this.props.toggleLoading();
    };

    handleSubmit = () => {
        const {isProcessing, toggleRegistration} = this.props;

        if (!isProcessing && !this.formValidation()) {
            toggleRegistration();
            fetch('http://localhost:3002/register', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(this.getRequestData()),
            })
                .then(resp => resp.json())
                .then(
                    resp => {
                        if (resp.errors) {
                            const error = resp.errors[0] || {},
                                {param: name, msg: message} = error;

                            if (name && message) {
                                this.setState({
                                    formError: {
                                        name,
                                        message,
                                    },
                                });
                            }
                        } else {
                            this.setState({isSnackBarOpen: true});
                        }
                        toggleRegistration();
                    },
                    error => {
                        this.setState({error});
                        toggleRegistration();
                    },
                );
        }
    };

    getRequestData = () => {
        const {name, dialCode, phone, country, email, password, passwordConfirmation, checkbox} = this.state,
            countryCode = this.getCountryCode(country);

        return {
            name,
            dialCode,
            phone,
            email,
            password,
            country: countryCode,
            passwordConfirmation,
            checkbox,
        };
    };

    formValidation = () => {
        let formError;

        const {name, dialCode, phone, country, email, password, passwordConfirmation} = this.state,
            passwordRegExp = /^[a-zA-Z_0-9]{5,128}$/,
            countryCode = this.getCountryCode(country);

        if (!/[a-zA-Z-]{3,30}/.test(name)) {
            formError = {
                name: 'name',
                message: 'Name must be more than 3 characters and less than 30',
            };
        } else if (!dialCode || !/^[0-9]+$/.test(dialCode)) {
            formError = {
                name: 'dialCode',
                message: 'Incorrect code',
            };
        } else if (!phone || !/^[0-9]{3,}$/.test(phone)) {
            formError = {
                name: 'phone',
                message: 'Incorrect phone',
            };
        } else if (!countryCode) {
            formError = {
                name: 'country',
                message: 'You must choose the country from the list',
            };
        } else if (!/^\S+@\S+\.\S+$/.test(email)) {
            formError = {
                name: 'email',
                message: 'Incorrect email',
            };
        } else if (!passwordRegExp.test(password)) {
            formError = {
                name: 'password',
                message: 'Incorrect password',
            };
        } else if (passwordConfirmation !== password || !passwordRegExp.test(passwordConfirmation)) {
            formError = {
                name: 'passwordConfirmation',
                message: 'Incorrect password confirm',
            };
        }

        if (formError) {
            this.setState({formError});
        }

        return formError;
    };

    getCountryCode = countryName => {
        const country = this.state.countries.find(el => el.name === countryName);

        return country ? country.country_code : '';
    };

    componentDidMount() {
        this.toggleLoading();
        fetch('http://localhost:3002/countries')
            .then(resp => resp.json())
            .then(
                countries => {
                    this.setState({countries});
                    this.toggleLoading();
                },
                error => {
                    this.setState({error});
                    this.toggleLoading();
                },
            );
    }

    handleChange = (event, data = {}) => {
        if (event) {
            const name = event.target.name || data.name;

            let value;

            if (name === 'checkbox') {
                value = event.target.checked;
            } else {
                value = event.target.value || data.value;
            }

            this.setState({[name]: value, formError: {}});
        }
    };

    getCodes = () => {
        return this.state.countries.map(el => {
            return {name: el.dial_code && el.dial_code.toString()};
        });
    };

    toggleSnackBar = () => {
        this.setState({isSnackBarOpen: !this.state.isSnackBarOpen});
    };

    getCheckBoxText() {
        return 'Yes, I\'d like to receive the very occasional email with information on new services and discounts';
    }

    isError = name => {
        return this.state.formError.name === name;
    };

    getError = name => {
        const {formError} = this.state;

        if (formError.name === name) {
            return formError.message;
        }
    };

    render() {
        const {countries, isSnackBarOpen} = this.state,
            {isLoading} = this.props;

        let content;

        if (isLoading) {
            content = (
                <React.Fragment>
                    <SnackBar
                        open={isSnackBarOpen}
                        onClose={this.toggleSnackBar}
                    >
                        <b>Great!</b>
                        <div>Your account has been successfully created</div>
                    </SnackBar>
                    <div className="app">
                        <div className="header">Sign up</div>
                        <div className="label">
                            <TextInput
                                label="Name"
                                name="name"
                                onChange={e => this.handleChange(e)}
                                error={this.isError('name')}
                            />
                            <ErrorComponent error={this.getError('name')}/>
                        </div>
                        <div className="label">
                            <div className="label-phone">
                                <div className="dialCode">
                                    <Select
                                        name="dialCode"
                                        suggestions={this.getCodes()} label="Code"
                                        onChange={(e, value) => this.handleChange(e, {value, name: 'dialCode'})}
                                        error={this.isError('dialCode')}
                                    />
                                </div>
                                <div className="phone">
                                    <TextInput
                                        name="phone"
                                        label="Phone number"
                                        onChange={e => this.handleChange(e)}
                                        error={this.isError('phone')}
                                    />
                                </div>
                            </div>
                            <ErrorComponent error={this.getError('dialCode')}/>
                            <ErrorComponent error={this.getError('phone')}/>
                        </div>
                        <div className="label">
                            <TextInput
                                name="email"
                                label="Email address"
                                onChange={e => this.handleChange(e)}
                                error={this.isError('email')}
                            />
                            <ErrorComponent error={this.getError('email')}/>
                        </div>
                        <div className="label">
                            <Select
                                name="country"
                                suggestions={countries}
                                label="Select a country"
                                placeholder='Start typing for search'
                                onChange={(e, value) => this.handleChange(e, {value, name: 'country'})}
                                error={this.isError('country')}
                            />
                            <ErrorComponent error={this.getError('country')}/>
                        </div>
                        <div className="label">
                            <TextInput
                                name="password"
                                label="Password"
                                type="password"
                                onChange={e => this.handleChange(e)}
                                error={this.isError('password')}
                            />
                            <ErrorComponent error={this.getError('password')}/>
                        </div>
                        <div className="label">
                            <TextInput
                                name="passwordConfirmation"
                                label="Password confirmation"
                                type="password"
                                onChange={e => this.handleChange(e)}
                                error={this.isError('passwordConfirmation')}
                            />
                            <ErrorComponent error={this.getError('passwordConfirmation')}/>
                        </div>
                        <Checkbox
                            name="checkbox"
                            label={this.getCheckBoxText()}
                            onChange={e => this.handleChange(e)}
                        />
                        <Button onClick={this.handleSubmit}>
                            Create an account
                        </Button>
                        <div className="info">
                            <span>Already have a 24Slides account? </span>
                            <a href="#">Click here</a> to log in to your existing account and join a company team
                        </div>
                    </div>
                </React.Fragment>
            );
        } else {
            content = <Loader/>;
        }

        return content;
    }

}

App.propTypes = {
    isLoading: PropTypes.bool,
    isProcessing: PropTypes.bool,
    toggleLoading: PropTypes.func,
    toggleRegistration: PropTypes.func,
};

const mapStateToProps = state => ({
        isLoading: state.loadingInProcess,
        isProcessing: state.registrationInProcess,
    }),
    mapDispatchToProps = dispatch => ({
        toggleLoading: () => dispatch(toggleLoading()),
        toggleRegistration: () => dispatch(toggleRegistration()),
    }),
    ConnectedApp = connect(
        mapStateToProps,
        mapDispatchToProps,
    )(App);


export default hot(module)(ConnectedApp);
