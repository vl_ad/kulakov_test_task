import React from 'react';
import AutoSuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import {makeStyles} from '@material-ui/core/styles';

function renderInputComponent(inputProps) {
    const {inputRef = () => {}, ref, classes, ...rest} = inputProps;

    return (
        <TextField
            fullWidth
            InputProps={{
                inputRef: node => {
                    ref(node);
                    inputRef(node);
                },
            }}
            {...rest}
        />
    );
}

function renderSuggestion(suggestion, {query, isHighlighted}) {
    const matches = match(suggestion.name, query),
        parts = parse(suggestion.name, matches);

    return (
        <MenuItem selected={isHighlighted} component="div">
            <div>
                {parts.map(part => (
                    <span key={part.text}>
                        {part.text}
                    </span>
                ))}
            </div>
        </MenuItem>
    );
}

function getSuggestions(value, suggestions) {
    const inputValue = value.trim().toLowerCase(),
        inputLength = inputValue.length;
    let count = 0;

    return inputLength === 0 ?
        [] :
        suggestions.filter(suggestion => {
            const keep =
                count < 7 && suggestion.name.toString().slice(0, inputLength).toLowerCase() === inputValue;

            if (keep) {
                count += 1;
            }

            return keep;
        });
}

const useStyles = makeStyles(theme => ({
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    },
    container: {
        position: 'relative',
    },
    suggestionsContainerOpen: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing(1),
        left: 0,
        right: 0,
    },
}));

export default function IntegrationAutoSuggest(props) {
    const classes = useStyles(),
        {suggestions, onChange, placeholder, label, name, error} = props,
        [state, setState] = React.useState({
            single: '',
            popper: '',
        }),

        [stateSuggestions, setSuggestions] = React.useState([]),

        handleSuggestionsFetchRequested = ({value}) => {
            setSuggestions(getSuggestions(value, suggestions));
        },

        handleSuggestionsClearRequested = () => {
            setSuggestions([]);
        },

        handleChange = name => (event, {newValue}) => {
            setState({
                ...state,
                [name]: newValue,
            });

            if (onChange) {
                onChange(event, newValue);
            }
        },

        autoSuggestProps = {
            renderInputComponent,
            suggestions: stateSuggestions,
            onSuggestionsFetchRequested: handleSuggestionsFetchRequested,
            onSuggestionsClearRequested: handleSuggestionsClearRequested,
            getSuggestionValue: suggestion => suggestion.name,
            renderSuggestion,
        };

    return (
        <div className={classes.root}>
            <AutoSuggest
                {...autoSuggestProps}
                inputProps={{
                    classes,
                    placeholder,
                    label,
                    value: state.single,
                    name,
                    onChange: handleChange('single'),
                    error,
                }}
                theme={{
                    container: classes.container,
                    suggestionsContainerOpen: classes.suggestionsContainerOpen,
                    suggestionsList: classes.suggestionsList,
                    suggestion: classes.suggestion,
                }}
                renderSuggestionsContainer={options => (
                    <Paper {...options.containerProps} square>
                        {options.children}
                    </Paper>
                )}
            />
        </div>
    );
}
