import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import '../../styles/button.scss';

class Button extends Component {
    render() {
        const {children, isProcessing, dispatch, ...rest} = this.props;

        return (
            <div
                className="button"
                {...rest}
            >
                {isProcessing ? <div className="loading"/> : children}
            </div>
        );
    }

}

Button.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
    isProcessing: PropTypes.bool,
    dispatch: PropTypes.func,
};

const mapStateToProps = state => ({
        isProcessing: state.registrationInProcess,
    }),
    ConnectedButton = connect(mapStateToProps)(Button);


export default ConnectedButton;
