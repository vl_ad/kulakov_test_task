import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import PropTypes from 'prop-types';

import '../../styles/checkbox.scss';

function CheckboxLabels(props) {
    const {checked, value, label, ...rest} = props;

    return (
        <FormControlLabel
            control={
                <Checkbox
                    checked={checked}
                    value={value}
                    {...rest}
                />
            }
            label={label}
        />
    );
}

CheckboxLabels.propTypes = {
    checked: PropTypes.bool,
    value: PropTypes.string,
    label: PropTypes.string,
};

export default CheckboxLabels;
