import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import PropTypes from 'prop-types';

import '../../styles/snackbar.scss';

function CustomizedSnackbars(props) {
    const {open, children, onClose} = props;

    return (
        <Snackbar
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            open={open}
            autoHideDuration={3000}
            onClose={onClose}
        >
            <SnackbarContent
                message={
                    <div className="message">
                        <div className="icon"/>
                        <div>
                            {children}
                        </div>
                    </div>
                }
                action={[
                    <div key="close" className="close" onClick={onClose}/>,
                ]}
            />
        </Snackbar>
    );
}

CustomizedSnackbars.propTypes = {
    open: PropTypes.bool,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
    onClose: PropTypes.func,
};

export default CustomizedSnackbars;
