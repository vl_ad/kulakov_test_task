import React from 'react';
import PropTypes from 'prop-types';

import '../../styles/errorComponent.scss';

function Error(props) {
    const {error} = props;

    return (
        error ? (
            <div className="error-component">
                {error}
            </div>
        ) : null
    );
}

Error.propTypes = {
    error: PropTypes.string,
};

export default Error;
