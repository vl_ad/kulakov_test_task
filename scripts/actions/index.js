import {TOGGLE_LOADING, TOGGLE_REGISTRATION} from './constants';

export const toggleRegistration = payload => ({
        type: TOGGLE_REGISTRATION,
        payload,
    }),
    toggleLoading = payload => ({
        type: TOGGLE_LOADING,
        payload,
    });
